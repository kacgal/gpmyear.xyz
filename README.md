## [gpmyear.xyz](https://gpmyear.xyz)

##### To use:
1. Visit https://takeout.google.com/
2. Click the "SELECT NONE" button
3. Scroll down to "My Activity"
4. Toggle the toggle on
5. Click on the arrow on the same row
6. Select the "Select specific activity data" radio button
7. In the dialog that pops up, first un-check "Toggle all"
8. Then check "Google Play Music"
9. And press OK
10. Change "HTML" to "JSON" in the drop-down
11. Scroll to the bottom, and click "NEXT", then "CREATE ARCHIVE"
12. Sit back, while Google searches their archives
13. Download the takeout, and extract it
14. In it, you'll find `MyActivity.json`, which is the file you upload to https://gpmyear.xyz/

---

##### Building:
1. Set your last.fm key in config.ts. You can get one at https://www.last.fm/api/account/create
3. Install dependencies with `npm install`
2. Run `make` in the root directory, the website will be output in the `dist` directory