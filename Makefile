DEST=dist
TSC=./node_modules/.bin/tsc

build:
	${TSC} --outDir ${DEST}
	cp *.html *.css ${DEST}/

clean:
	rm -rf ${DEST}

all: clean build

.PHONY: clean build