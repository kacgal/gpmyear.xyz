type HumanizeDurationUnit = 's' | 'm' | 'h' | 'd' | 'w' | 'M' | 'y'

interface HumanizeDurationOptions {
    units?: HumanizeDurationUnit[],
    round?: boolean
}

interface HumanizeDuration {
    (seconds: number, humanizeDurationOptions?: HumanizeDurationOptions): string;
}

declare const humanizeDuration: HumanizeDuration;

