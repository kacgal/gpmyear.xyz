declare interface WorkerMessage {
    id: number,
    total: number,
    i: number,
    done: boolean,
    leftEstimate: number
}

declare interface TrackInfo {
    title: string,
    artist: string
}

declare interface SearchMessage {
    toSearch: TrackInfo[],
    counts: { [s: string]: number },
    id: number
}