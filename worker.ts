importScripts('config.js');

onmessage = (e) => {

    // @ts-ignore
    if (LASTFM_KEY === '') {
        // @ts-ignore
        postMessage(null);
        return;
    }

    const db = indexedDB.open('last.fm');

    db.onupgradeneeded = () => {
        db.result.createObjectStore('lastfm_cache', { keyPath: ['artist', 'track'] });
    };

    db.onsuccess = () => {
        const data = <SearchMessage>e.data;

        let total = 0;
        let i = 0;

        function next() {
            const s = data.toSearch[i++];

            getTrackInfo(db.result, s, (length) => {
                if (length !== 0) {
                    total += length * data.counts[`${s.title} - ${s.artist}`];
                    // @ts-ignore
                    postMessage({
                        id: data.id,
                        i: i,
                        total: total,
                        done: i === data.toSearch.length,
                        leftEstimate: data.toSearch
                            .slice(i)
                            .map((e) => 1000 * 60 * 4 * data.counts[`${e.title} - ${e.artist}`])
                            .reduce((a, b) => a + b, 0)
                    });
                }
                if (i < data.toSearch.length) {
                    next();
                }
            });
        }
        next();
    };
};

interface TrackLength {
    artist: string,
    track: string,
    length: number
}

interface LastFMTrackInfo {
    duration: string
}

interface LastFMTrackResponse {
    error?: number,
    message?: string,
    track: LastFMTrackInfo
}

function getTrackInfo(db: IDBDatabase, track: TrackInfo, cb: (length: number) => void, backoff: number = 500) {
    const url = `https://ws.audioscrobbler.com/2.0/?method=track.getInfo&format=json&api_key=${LASTFM_KEY}&track=${encodeURIComponent(track.title)}&artist=${encodeURIComponent(track.artist)}`;
    const t1 = db.transaction('lastfm_cache', 'readonly');
    const s1 = t1.objectStore('lastfm_cache');

    function backOff() {
        setTimeout(() => {
            getTrackInfo(db, track, cb, backoff * 2);
        }, backoff);
    }

    const get = <IDBRequest<TrackLength>>s1.get([track.artist, track.title]);
    get.onsuccess = () => {
        if (get.result === undefined) {
            fetch(url)
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        backOff();
                    }
                })
                .then((json: LastFMTrackResponse) => {
                    if (json.error !== undefined) {
                        if (json.error === 6 && json.message === 'Track not found') {
                            cb(0);
                        } else {
                            backOff();
                        }
                    } else {
                        const t2 = db.transaction('lastfm_cache', 'readwrite');
                        const s2 = t2.objectStore("lastfm_cache");
                        const duration = parseInt(json.track.duration);
                        const insert = s2.put({artist: track.artist, track: track.title, length: duration});
                        insert.onsuccess = () => {
                            cb(duration);
                        };
                    }
                })
                .catch(() => {
                    backOff();
                });
        } else {
            cb(get.result.length);
        }
    };
}