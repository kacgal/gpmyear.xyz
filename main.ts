$(() => {
    let $start = $('#start');
    $start.modal('show');
    const now = new Date();
    const $year = $('#year');
    $year.append(
        Array.from(
            new Array(now.getUTCFullYear() - 2011 + 1), (_, i) => i + 2011)
            .reverse()
            .map(year => $('<option />').attr('value', year).text(year)));
    if(now.getUTCMonth() <= 6) {
        $year.val(now.getUTCFullYear() - 1);
    }

    $('#dark_theme').on('change', (e: JQuery.ChangeEvent) => {
        localStorage.setItem('dark_theme', (<HTMLInputElement>e.target).checked.toString());
        updateTheme();
    });
    updateTheme();

    $('body').on('keyup', (e: JQuery.KeyUpEvent) => {
        if (map !== null && e.key === 'Escape') {
            $start.modal('toggle');
        }
    });

    $('#file_select').on('change', (e: JQuery.ChangeEvent) => {
        handleFile((<HTMLInputElement>e.target).files[0], actions());
    });

    let resizing = false;

    $('#map_resize')
        .on('mousedown', () => {
            resizing = true;
        });

    const $document = $(document);

    $('#map').on('wheel', () => {
        $document.scrollTop(0);
    });

    $document
        .on('selectstart', (e: JQuery.Event) => {
            if (resizing) {
                e.preventDefault();
            }
        })
        .on('scroll', () => {
            if (resizing) {
                $document.scrollTop(0);
            }
        })
        .on('mousemove', (e: JQuery.MouseMoveEvent) => {
            if (resizing) {
                e.preventDefault();
                $('#map').height(Math.min(e.pageY, window.innerHeight - $('#map_resize').height()));
                map.updateSize();
                map.getView().fit(heatmap.getSource().getExtent(), { size: map.getSize() });
            }
        })
        .on('mouseup', () => {
            resizing = false;
        });
});

function updateTheme() {
    const useDark = localStorage.getItem('dark_theme') === 'true';
    const $bg = $('body');
    if (useDark) {
        $bg.addClass('dark');
    } else {
        $bg.removeClass('dark');
    }

    if (tileSource !== null) {
        const mapTheme = useDark ? 'dark' : 'light';
        tileSource.setUrl(`https://{a-d}.basemaps.cartocdn.com/${mapTheme}_all/{z}/{x}/{y}.png`);
    }
    $('#dark_theme').prop('checked', useDark);
}

function handleDrop(event: DragEvent) {
    event.preventDefault();

    const transfer = event.dataTransfer;

    if (transfer.files.length === 0) return;

    handleTransfer(transfer, actions());
}

const actions = () => {
    const $progress = $('#progress-text');
    const $start = $('#start');
    const $processing = $('#processing');

    $start.modal('hide');
    $processing.modal('show');

    return {
        progress: (p) => {
            $progress.text(p);
        },
        error: (error, debug) => {
            if (error !== undefined) {
                if (debug !== undefined) {
                    prompt(`${error} Contact /u/kacgal, and send them the following:`, debug);
                } else {
                    alert(error);
                }
            }
            $start.modal('show');
            $processing.modal('hide');
        },
        hideProgress: () => {
            $processing.modal('hide');
        },
        showProgress: () => {
            $processing.modal('show');
        },
        done: () => {
            $processing.modal('hide');
            $('#result').show();
        }
    };
};

interface Actions {
    progress: (progress: string) => void,
    error: (error?: string, debug?: string) => void,
    hideProgress: () => void,
    showProgress: () => void
    done: () => void;
}

function handleTransfer(transfer: DataTransfer, actions: Actions) {
    actions.progress('Checking file...');
    if (transfer.files.length > 1) {
        actions.error('Please drop ony one file');
    } else if (transfer.items[0].kind !== 'file') {
        actions.error('Please drop only files');
    } else if (transfer.files[0].name.search('\.json$') === -1) {
        actions.error('Please only choose .json files');
    } else if (transfer.files[0].name !== 'MyActivity.json' && !confirm('Are you sure you dropped the right file? It should be called MyActivity.json')) {
        actions.error();
    } else {
        handleFile(transfer.files[0], actions);
    }
}

function handleFile(file: File, actions: Actions) {
    actions.progress('Loading file...');
    const reader = new FileReader();
    reader.addEventListener('load', () => {
        parseJSON(<string>reader.result, actions);
    });
    reader.readAsText(file);
}

interface GPMLocation {
    name: string,
    url: string
}

interface GPMActivity {
    header: string,
    title: string,
    description?: string,
    titleUrl: string,
    time: string,
    products: string[],
    locations?: GPMLocation[]
}

function parseJSON(str: string, actions: Actions) {
    actions.progress('Parsing file...');
    try {
        handleJSON(<GPMActivity[]>JSON.parse(str), actions);
    } catch (e) {
        actions.error('Could not parse the file!', e.message);
    }
}

function handleJSON(json: GPMActivity[], actions: Actions) {
    const year = parseInt(<string>$('#year').val());

    actions.progress('Counting...');
    const songs: { [s: string]: number } = {};
    const artists: { [s: string]: number } = {};
    const locations: { [s: string]: number } = {};
    const toSearch: GPMActivity[] = [];

    for (const entry of json) {
        if (entry.description === undefined) continue;
        if (new Date(entry.time).getUTCFullYear() !== year) continue;

        const title = `${entry.title} - ${entry.description}`;

        if (!(title in songs)) {
            toSearch.push(entry);
            songs[title] = 0;
        }
        songs[title]++;

        if (!(entry.description in artists)) {
            artists[entry.description] = 0;
        }
        artists[entry.description]++;

        if (entry.locations !== undefined) {
            for (const loc of entry.locations) {
                const coords = new URL(loc.url).searchParams.get('q');
                if (!(coords in locations)) {
                    locations[coords] = 0;
                }
                locations[coords]++;
            }
        }
    }

    if (Object.keys(songs).length === 0) {
        actions.error(`Seems like there's no data for ${year}. Try picking another year?`);
    } else {
        getTranslatedPrefixes(Object.keys(songs), (listenedTo, skipped) => {
            const listenedToSongs: { [s: string]: number } = {};
            const skippedSongs: { [s: string]: number } = {};

            for (const song of Object.keys(songs)) {
                if (song.startsWith(listenedTo)) {
                    listenedToSongs[song.substr(listenedTo.length)] = songs[song];
                } else if (song.startsWith(skipped)) {
                    skippedSongs[song.substr(skipped.length)] = songs[song];
                }
            }
            startWorker(toSearch
                .filter((a) => a.title.startsWith(listenedTo))
                .sort((a, b) => songs[`${b.title} - ${b.description}`] - songs[`${a.title} - ${a.description}`])
                .map((a) => {
                    return {
                        title: a.title.substr(listenedTo.length),
                        artist: a.description
                    };
                }), listenedToSongs);
            processCounts(listenedToSongs, skippedSongs, artists, locations, actions);
        }, actions);
    }
}

function processCounts(
    listenedToSongs: { [s: string]: number }, skippedSongs: { [s: string]: number },
    artists: { [s: string]: number },
    locations: { [s: string]: number },
    actions: Actions) {

    actions.progress('Sorting everything...');
    const sortedListenedTo = Object.keys(listenedToSongs).sort((a, b) => listenedToSongs[b] - listenedToSongs[a]);
    const sortedArtists = Object.keys(artists).sort((a, b) => artists[b] - artists[a]);
    const sortedSkipped = Object.keys(skippedSongs).sort((a, b) => skippedSongs[b] - skippedSongs[a]);

    actions.progress('Creating result...');
    $('#top-artists-list').empty().append(sortedArtists.map((a) => $('<li />').text(`${a} (${artists[a]})`)));
    $('#top-songs-list').empty().append(sortedListenedTo.map((a) => $('<li />').text(`${a} (${listenedToSongs[a]})`)));
    $('#top-skipped-songs-list').empty().append(sortedSkipped.map((a) => $('<li />').text(`${a} (${skippedSongs[a]})`)));

    actions.done();

    makeMap(locations);
}

let heatmap: ol.layer.Heatmap | null = null;
let tileSource: ol.source.XYZ | null = null;
let map: ol.Map | null = null;

function makeMap(locations: { [s: string]: number }) {
    const weights: Map<ol.Feature, number> = new Map();
    const max = Math.max(...Object.values(locations));
    const vector = new ol.source.Vector({
        features: Object.entries(locations)
            .map(([loc, count]) => {
                const coords = (<string>loc).split(',').map(parseFloat);
                const feature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([coords[1], coords[0]])));
                weights.set(feature, Math.min(count / max + 0.40, 1));
                return feature;
            })
    });
    if (heatmap === null) {
        heatmap = new ol.layer.Heatmap({
            source: vector,
            weight: (feature) => weights.get(feature)
        });
    }
    if (tileSource === null) {
        const mapTheme = localStorage.getItem('dark_theme') === 'true' ? 'dark' : 'light';
        tileSource = new ol.source.XYZ({
            url: `https://{a-d}.basemaps.cartocdn.com/${mapTheme}_all/{z}/{x}/{y}.png`,
            maxZoom: 19,
            attributions: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="https://carto.com/attributions">CARTO</a>'
        });
    }
    if (map === null) {
        map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: tileSource
                }),
                heatmap
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([0, 0]),
                zoom: 4
            })
        });
    }
    heatmap.setSource(vector);
    if (Object.entries(locations).length === 0) {
        $('#map, #map_resize').hide();
    } else {
        $('#map, #map_resize').show();
        map.getView().fit(vector.getExtent(), { size: map.getSize() });
    }
}

const worker = new Worker('worker.js');
let wid = 0;
worker.onmessage = debounce((e) => {
    const data = <WorkerMessage>e.data;
    if (data === null) {
        $('#listen').hide();
        return;
    }
    if (data.id < wid) return;
    $('#song_i').text(data.i);
    $('#listen_length').text(humanizeDuration(data.total));
    $('#listen_length_minutes').text(humanizeDuration(data.total, { units: ['m'], round: true }));
    $('#listen_estimate').text(humanizeDuration(data.total + data.leftEstimate));
    if (data.done) {
        $('#song_counter').hide();
    }
}, 250);

function debounce(fn: (...args: any[]) => any, interval: number): ((...args: any[]) => void) {
    let args = null;
    setInterval(() => {
        if (args !== null) {
            const tmpArgs = args;
            args = null;
            fn(...tmpArgs);
        }
    }, interval);
    return (...a) => {
        args = a;
    };
}

function startWorker(toSearch: TrackInfo[], counts: { [s: string]: number }) {
    $('#song_counter').show();
    $('#song_total').text(toSearch.length);
    worker.postMessage({ toSearch: toSearch, counts: counts, id: ++wid });
}

const translations = {
    'Listened to ': 'Skipped ',
    'Angehört ': 'Übersprungen '
};

function getTranslatedPrefixes(strings: string[], cb: (listenedTo: string, skipped: string) => void, actions: Actions) {
    const sorted = strings.sort();

    for (const transation of Object.keys(translations)) {
        if (sorted[0].startsWith(transation)) {
            cb(transation, translations[transation]);
            return;
        } else if (sorted[sorted.length - 1].startsWith(transation)) {
            cb(translations[transation], transation);
            return;
        }
    }

    let array = sorted;
    while (sharedStart(array).length === 0) {
        array = array.slice(0, -1);
    }

    const prefix1 = sharedStart(array);
    const prefix2 = sharedStart(sorted.slice(array.length, sorted.length - 1));

    let $languagePrompt = $('#language-prompt');

    $('#translation-string').text(prefix1);

    actions.hideProgress();
    $languagePrompt.modal('show');

    $('#means-listened-to, #means-skipped').one('click', (e) => {
        actions.showProgress();
        $languagePrompt.modal('hide');
        let id = $(e.target).attr('id');
        switch (id) {
            case 'means-listened-to':
                cb(prefix1, prefix2);
                break;
            case 'means-skipped':
                cb(prefix2, prefix1);
                break;
            default:
                actions.error('Oops, something went wrong!', id);
        }
    });
}

// https://stackoverflow.com/a/1917041/1469722
function sharedStart(array) {
    if (array.length === 0) return null;
    var A = array.concat().sort(),
        a1 = A[0], a2 = A[A.length - 1], L = a1.length, i = 0;
    while (i < L && a1.charAt(i) === a2.charAt(i)) i++;
    return a1.substring(0, i);
}